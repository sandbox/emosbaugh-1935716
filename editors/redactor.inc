<?php

/**
 * @file
 * Editor integration functions for Redactor.
 */

/**
 * Plugin implementation of hook_editor().
 */
function wysiwyg_redactor_redactor_editor() {
  $editor['redactor'] = array(
    'title' => 'Redactor',
    'vendor url' => 'http://imperavi.com/redactor/',
    'download url' => 'http://imperavi.com/redactor/download/',
    'libraries' => array(
      '' => array(
        'title' => 'Default',
        'files' => array(
          'redactor.min.js' => array('preprocess' => FALSE),
        ),
      ),
      'src' => array(
        'title' => 'Source',
        'files' => array(
          'redactor.js' => array('preprocess' => FALSE),
        ),
      ),
    ),
    'version callback' => 'wysiwyg_redactor_version',
    'settings form callback' => 'wysiwyg_redactor_settings_form',
    'init callback' => 'wysiwyg_redactor_init',
    'settings callback' => 'wysiwyg_redactor_settings',
    'plugin callback' => 'wysiwyg_redactor_plugins',
    'css path' => wysiwyg_get_path('redactor'),
    'versions' => array(
      '8.2.2' => array(
        'js files' => array('redactor.js'),
        'css files' => array('redactor.css'),
      ),
    ),
  );
  return $editor;
}

/**
 * Detect editor version.
 *
 * @param $editor
 *   An array containing editor properties as returned from hook_editor().
 *
 * @return
 *   The installed editor version.
 */
function wysiwyg_redactor_version($editor) {
  $library = $editor['library path'] . '/redactor.js';
  if (!file_exists($library)) {
    return;
  }
  $library = fopen($library, 'r');
  $max_lines = 3;
  while ($max_lines && $line = fgets($library, 500)) {
    if (preg_match('/Redactor v(\d+\.\d+\.\d+)/', $line, $version)) {
      fclose($library);
      return $version[1];
    }
    $max_lines--;
  }
  fclose($library);
}

/**
 * Removes the editor profile settings that are not supported by Redactor.
 */
function wysiwyg_redactor_settings_form(&$form, &$form_state) {
  $form['appearance']['#access'] = FALSE;
  $form['css']['css_classes']['#access'] = FALSE;
  $form['output']['#access'] = FALSE;

  unset($form['buttons']['#theme']);
  foreach (element_children($form['buttons']) as $name) {
    $form['buttons'][$name]['#access'] = FALSE;
  }

  $default_buttons = "html,|,formatting,|,bold,italic,deleted,|,unorderedlist,orderedlist,outdent,indent,|,image,video,file,table,link,|,fontcolor,backcolor,|,alignment,|,horizontalrule";

  $plugins = wysiwyg_get_plugins('redactor');
  $profile = $form_state['wysiwyg_profile'];
  $form['buttons']['redactor']['defaults'] = array(
    '#type' => 'textarea',
    '#title' => t('Buttons'),
    '#default_value' => !empty($profile->settings['buttons']['redactor']['defaults']) ? $profile->settings['buttons']['redactor']['defaults'] : $default_buttons,
    '#description' => t('Separate buttons with commas. Available buttons: @buttons', array('@buttons' => '"'. implode('", "', array_keys($plugins['default']['buttons'])) .'"')),
  );
}

/**
 * Returns an initialization JavaScript for this editor library.
 *
 * @param array $editor
 *   The editor library definition.
 * @param string $library
 *   The library variant key from $editor['libraries'].
 * @param object $profile
 *   The (first) wysiwyg editor profile.
 *
 * @return string
 *   A string containing inline JavaScript to execute before the editor library
 *   script is loaded.
 */
function wysiwyg_redactor_init($editor) {
  // CKEditor unconditionally searches for its library filename in SCRIPT tags
  // on the page upon loading the library in order to determine the base path to
  // itself. When JavaScript aggregation is enabled, this search fails and all
  // relative constructed paths within CKEditor are broken. The library has a
  // CKEditor.basePath property, but it is not publicly documented and thus not
  // reliable. The official documentation suggests to solve the issue through
  // the global window variable.
  // @see http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Specifying_the_Editor_Path
  $library_path = base_path() . $editor['library path'] . '/';
  return <<<EOL
window.REDACTOR_BASEPATH = '$library_path';
EOL;
}

/**
 * Return runtime editor settings for a given wysiwyg profile.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $config
 *   An array containing wysiwyg editor profile settings.
 * @param $theme
 *   The name of a theme/GUI/skin to use.
 *
 * @return
 *   A settings array to be populated in
 *   Drupal.settings.wysiwyg.configs.{editor}
 */
function wysiwyg_redactor_settings($editor, $config, $theme) {
  global $language;

  $settings = array(
    'minHeight' => 300,
    'lang' => isset($config['language']) ? $config['language'] : $language->language,
  );

  $allowedTags = array('p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6');
  $settings['formattingTags'] = array_intersect(explode(',', $config['block_formats']), $allowedTags);

  if (isset($config['buttons']['redactor']['defaults'])) {
    $settings['buttons'] = explode(',', $config['buttons']['redactor']['defaults']);
  }

  switch ($config['css_setting']) {
    case 'none':
      $settings['iframe'] = TRUE;
    case 'self':
      if (isset($config['css_path'])) {
        $settings['css'] = strtr($config['css_path'], array('%b' => base_path(), '%t' => drupal_get_path('theme', variable_get('theme_default', NULL))));
      }
      break;
  }

  if (module_exists('redactor_wysiwyg_bridge')) {
    if (user_access('upload images')) {
      $settings['imageUpload'] = url('redactor/imageupload');
    }
    if (user_access('upload files')) {
      $settings['fileUpload'] = url('redactor/fileupload');
    }
  }

  //print_r($config);
  //print_r($settings); exit;
  return $settings;
}

/**
 * Return internal plugins for this editor; semi-implementation of hook_wysiwyg_plugin().
 */
function wysiwyg_redactor_plugins($editor) {
  $plugins = array(
    'default' => array(
      'buttons' => array(
        '|' => t('Separator'),
        'html' => t('HTML'),
        'formatting' => t('HTML block format'),
        'bold' => t('Bold'),
        'italic' => t('Italic'),
        'deleted' => t('Strike-through'),
        'unorderedlist' => t('Bullet list'),
        'orderedlist' => t('Numbered list'),
        'outdent' => t('Outdent'),
        'indent' => t('Indent'),
        'image' => t('Image'),
        'video' => t('Embed'),
        'file' => t('File'),
        'table' => t('Table'),
        'link' => t('Link'),
        'fontcolor' => t('Forecolor'),
        'backcolor' => t('Backcolor'),
        'alignment' => t('Text alignment'),
        'horizontalrule' => t('Horizontal rule'),
        'underline' => t('Underline'),
        'alignleft' => t('Align left'),
        'aligncenter' => t('Align center'),
        'alignright' => t('Align right'),
        'justify' => t('Justify'),
      ),
    ),
  );
  return $plugins;
}
