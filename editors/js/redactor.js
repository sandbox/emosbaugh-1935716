var REDACTOR = REDACTOR || {instances: {}};

(function($) {

Drupal.wysiwyg.editor.init.redactor = function(settings) {
  // ???
};


/**
 * Attach this editor to a target element.
 */
Drupal.wysiwyg.editor.attach.redactor = function(context, params, settings) {
  // Attach editor.
  settings.formattingTags = $.map(settings.formattingTags, function (value, key) { return value; });
  REDACTOR.instances[params.field] = $('#'+params.field, context).redactor(settings);
};

/**
 * Detach a single or all editors.
 */
Drupal.wysiwyg.editor.detach.redactor = function (context, params, trigger) {
  if (typeof params != 'undefined') {
    $('#'+params.field, context).destroyEditor();
  } else {
    for (var instanceId in REDACTOR.instances) {
      $('#'+instanceId, context).destroyEditor();
    }
  }
};

})(jQuery);
